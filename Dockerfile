FROM node:14.15.0-alpine3.10

RUN apk add --update --no-cache python3 make g++ && ln -sf python3 /usr/bin/python


WORKDIR /api

COPY ./package.json .
COPY ./package-lock.json .

RUN npm i -g typescript node-tsc && npm i

COPY . .

EXPOSE 3000

CMD ["npm", "run", "prod"]
