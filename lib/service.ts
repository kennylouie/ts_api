import Route from './route'

interface IService {
  GetRoutes(): Route[];
}

export default IService;
