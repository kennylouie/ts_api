interface ILogger {
  Info(message: string): void;
}

export default ILogger;
