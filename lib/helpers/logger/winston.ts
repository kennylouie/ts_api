import {
  Logger,
  createLogger,
  transports,
  format,
} from "winston";

class WinstonNewRelic {
  private name: string;
  private logger: Logger;

  constructor(name: string) {
    this.name = name;
    this.logger = this.newLogger(name);
  }

  public Child(name: string): WinstonNewRelic {
    return new WinstonNewRelic(this.name + "." + name)
  }

  public Info(message: string): void {
    this.logger.info(message);
  }

  private newLogger(name: string): Logger {
    const newrelicFormatter = require('@newrelic/winston-enricher');

    const logger: Logger = createLogger({
      transports: [
        new transports.Console(),
      ],
      format: format.combine(
        format.label({label: name}),
        newrelicFormatter()
      )
    });

    return logger;
  }
}

export default WinstonNewRelic;
