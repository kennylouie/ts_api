class Conf {
  private port: number;

  // gets environment variables
  public Start(): void {
    this.port = +process.env["API_PORT"]
  }

  public getPort(): number {
    return this.port;
  }
}

export default Conf;
