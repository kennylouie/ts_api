import {Request, Response } from 'express';

interface Route {
  name: string;
  path: string;
  method: string;
  handler: ((req: Request, res: Response) => void);
}

export default Route;
