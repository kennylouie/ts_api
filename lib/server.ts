import 'newrelic';

import Api from "./api"
import Conf from "./conf"
import WinstonNewRelic from "./helpers/logger/winston"

import Version from "./services/version"
import Health from "./services/health"

function main(): void {
  // logger
  const logger = new WinstonNewRelic("tsc_api");

  // get configuration details
  let conf = new Conf();
  conf.Start();

  let api = new Api(logger.Child("api"));
  api.Configure()

  // init services
  let version = new Version();
  let health = new Health();

  // load services
  api
    .LoadRoutes(version)
    .LoadRoutes(health)

  // start server
  api.Start(conf.getPort())
}

main()
