import * as express from "express";
import * as bodyParser from "body-parser";

import IService from './service';
import ILogger from './helpers/logger/logger';
import Route from './route';

class Api {
  public api: express.Application;
  private logger: ILogger

  constructor(logger: ILogger) {
    this.api = express();
    this.logger = logger;
  }

  public Configure(): void {
    // support application/json type post data
    this.api.use(bodyParser.json());

    //support application/x-www-form-urlencoded post data
    this.api.use(bodyParser.urlencoded({ extended: false }));
  }

  public LoadRoutes(service: IService): this {
    let routes: Route[] = service.GetRoutes();

    for (let route of routes) {
      // evil line that should be refactored one day in a better way
      eval("this.api." + route.method + "(route.path, route.handler)")
    }

    return this;
  }

  public Start(port: number): void {
    this.api.listen(port, () => {
      this.logger.Info("Server started on port: " + port)
    })
  }
}

export default Api;
