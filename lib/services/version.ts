import {Request, Response } from 'express';

import Route from '../route';

class Version {
  private version: string;

  constructor() {
    this.ReadPackageJson();
  }

  private ReadPackageJson(): void {
    const pjson = require("../../package.json");

    this.version = pjson.version;
  }

  public GetRoutes(): Route[] {
    let routes: Route[] = [
      {
        name: "version",
        path: "/api/version",
        method: "get",
        handler: (req: Request, res: Response) => {
          res.status(200).json({version: this.version})
        },
      },
    ];

    return routes;
  }
}

export default Version;
