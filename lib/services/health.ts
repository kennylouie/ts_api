import {Request, Response } from 'express';

import Route from '../route';

const online: string = "online";

class Health {
  private status: string;

  constructor() {
    this.status = online;
  }

  public GetRoutes(): Route[] {
    let routes: Route[] = [
      {
        name: "health",
        path: "/api/health",
        method: "get",
        handler: (req: Request, res: Response) => {
          res.status(200).json({status: this.status})
        },
      },
    ];

    return routes;
  }
}

export default Health;
